<!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Форма</title>
	<style>
	   form {
	       width: 600px;
	       height: 700px;
	       background: SkyBlue;
	       border-radius: 8px;
	       margin: 0 auto;
	       padding: 30px;
	       box-shadow: 0px 0px 14px 0px rgba(46, 53, 55, 0.77);
	   }
	   p {
	       line-height: 0.5;
	   }
	   label {
	       margin: 3px;
	   }
	   input {
	       margin: 8px 0;
	   }
	   input[type="text"], input[type="email"] {
	       width: 100%;
	       height: 30px;
	       border-radius: 5px;
	       border: 2px solid grey;
	       outliine: none;
	       padding: 7px;
	   }
	   input[type="checkbox"] {
	       margin-right: 7px;
	   }
	   textarea {
	       width: 300px;
	       height: 150px;
	       padding: 7px;
	   }
	   input[type="submit"] {
	       padding: 7px 20px;
	       border-radius: 5px;
	       box-shadow: 0px 0px 5px 0px rgba(46, 53, 55, 0.5);
	   }
	   input[type="submit"]:hover {
	       cursor: pointer;
	   }
	</style>
</head>

<form action="index.php" method="POST">
  <label>Ваше имя</label>
  <input name="fio" type="text">
  <br>
  
  <label>Ваш email</label>
  <input name="email" type="email">
  <br>
  
  <p>Ваш день рождения </p>
  <select name="year">
  <?php for($i = 1930; $i < 2020; $i++) {?>
  	<option value="<?php print $i; ?>"><?= $i; ?></option>
  	<?php }?>
  </select>
  <br>
  
  <p>Ваш пол</p>
  <label class="radio">
		<input type="radio" name="sex" value="0" checked>
		Мужчина
  </label>
  <label class="radio">
		<input type="radio" name="sex" value="1">
		Женщина
  </label>
  <br>
  
  <p>Число конечостей</p>
  <label class="radio">
		<input type="radio" name="limbs" value="2" checked>
		2
  </label>
  <label class="radio">
		<input type="radio" name="limbs" value="4">
		4
  </label>
  <label class="radio">
		<input type="radio" name="limbs" value="6">
		6
  </label>
  <label class="radio">
		<input type="radio" name="limbs" value="8">
		8
  </label>
  <label class="radio">
		<input type="radio" name="limbs" value="10">
		10
  </label>
  <br>
  <br>
	
  <select name="abilities[]" multiple>
  	<option value="immort">Бессмертие</option>
  	<option value="wall">Хождения сквозь стены</option>
  	<option value="levit">Левитация</option>
  	<option value="invis">Невидимость</option>
  </select>
  <br>
  
  <p>Ваша биография</p>
  <textarea name="text" placeholder="Биография" rows=10 cols=30></textarea>
  <br>
  
  <input type="checkbox" name="accept" value="1">Принять
  <br>
  <input type="submit" value="Отправить">
</form>